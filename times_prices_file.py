import lib, datetime

sql_txt = '''
select
  t1.time,
  coalesce(t2.cnt, 0) as cnt
from
  (
    select
      t1.time::time
    from
      generate_series('2021-01-01 00:00:00'::timestamp, '2021-01-01 23:30:00', '30 min') as t1(time)
  ) as t1
    left join
  (
    select
      t1.time,
      count(*) as cnt
    from
      tbl.setting_price_timesheets as t1
        inner join
      tbl.login_states as t2
        on t2.login_id = t1.work_login_id
        and t2.work_contract_id = convert_to(t1.work_contract_id,'UTF8')

    where
      t1.time is not null 
      and t1.enabled
    group by
      t1.time
  ) as t2
  on t2.time = t1.time; 
    '''

now = datetime.datetime.now()
date = now.strftime('%d_%m_%y')

path_csv = 'csv/' + date +'.csv'
table_name = ['time','count']

lib.processing_data(sql_txt, path_csv, table_name)
