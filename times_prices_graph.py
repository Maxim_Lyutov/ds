import csv, os
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

def set_xy(file_name='', delta=0):
    x1, y1= [],[]
    with open(file_name) as file:
        reader = csv.DictReader(file,delimiter=';')
        for row in reader:
            time = (int(row['time'][:2])*60 + int(row['time'][3:5])) / 60
            x1.append(time+delta)

            #print(time+delta, int(row['count']))
            y1.append(int(row['count']))
            
    return [x1,y1]

fig, ax = plt.subplots()

ax.xaxis.set_major_locator(ticker.MultipleLocator(2))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(1))

ax.yaxis.set_major_locator(ticker.MultipleLocator(50))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(10))

list_of_files = []
path = r'csv'
for root, dirs, files in os.walk(path):
	for file in files:
		list_of_files.append(os.path.join(root,file))

k = 0
for file in list_of_files:
    #print(file)
    x1,y1 = (set_xy(file,k))
    ax.bar(x1, y1, label= file, width = 0.1)
    k += 0.05

plt.xlabel("время, ч.")
plt.ylabel("кол-во прайсов, шт.")
plt.title("Распредление прайсов по суткам.")
plt.legend()
plt.grid()
plt.show()

fig.savefig('Распредление прайсов по суткам.png', dpi = 600)