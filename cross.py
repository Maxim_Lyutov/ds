import psycopg2, lib, common

brand_name = 'knorr'

sql_txt ='''
select * from exchange.cross_export(
    (select
    id
    from
    tbl.brands br
    where
    br.name ilike '%{}%')::integer 
) '''.format(brand_name)

result = []

try:
    con = psycopg2.connect(database=common.pg_database ,
                           user=common.pg_user,
                           password=common.pg_password,
                           host=common.pg_host,
                           port=common.pg_port)
    cur = con.cursor()
    cur.execute(sql_txt)

    rows = cur.fetchall()
    for row in rows:
        result.append(row)
    con.close()

    cross_brand = lib.write_log(brand_name+'.csv')
    cross_brand.writerow(['left_brand_name', 'left_code', 'rigth_code', 'rigth_brand_name'] )
    
    for data in result:
        cross_brand.writerow(data)

except Exception as ex:
    print('error :' + ex)
