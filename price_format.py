import lib
sql_txt = '''
with cte as (
  select 
    file_format, 
    count(*) as cnt 
  from 
    tbl.setting_price_timesheets
  where 
    enabled 
  group by 
   file_format 
) 
select  * from cte '''

path_csv = 'price/price_format.csv'
table_name = ['cnt', 'file_format']

lib.processing_data(sql_txt, path_csv, table_name)

lib.create_graph(path_csv,'cnt','file_format', 'Распределение форматов прайсов(файлов)')