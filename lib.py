import matplotlib.pyplot as plt
import csv, os,psycopg2, common

def write_log(file_name):
    file = open(file_name, 'w',newline="", encoding="utf-8" ) # "utf-8" cp1251
    writer = csv.writer(file,delimiter = ';')
    return writer 

def set_xy_num(file_name='', x_name='',y_name=''):
    x1, y1= [],[]
    with open(file_name, encoding="utf-8") as file:
        reader = csv.DictReader(file,delimiter=';')
        for row in reader:
            x1.append(row[x_name])
            y1.append(row[y_name])
    
    return [x1,y1]

def create_graph(file='', x_name='', y_name='', title='test', type='pie'):

    fig, ax = plt.subplots()
    x1, y1= [],[]

    x1,y1 = (set_xy_num(file, x_name, y_name))
    if type == 'pie':
        ax.pie(y1, startangle=45, autopct="%0.2f%%", radius=1.4, wedgeprops=dict(width=0.98), rotatelabels=True)
        
    else :
        ax.barh(x1,y1)
    
    plt.legend(x1)
    plt.axis('equal')
    plt.title(title)
    plt.show()

    fig.savefig(title +'.png', dpi = 600)

def processing_data(sql_txt='', path_to_csv='pay/order_segment.csv',name_table=['order', 'segment']):
    lst = []
    try:
        con = psycopg2.connect(database=common.pg_database ,
                            user=common.pg_user,
                            password=common.pg_password,
                            host=common.pg_host,
                            port=common.pg_port)
        cur = con.cursor()
        cur.execute(sql_txt)

        rows = cur.fetchall()
        for row in rows:
            lst.append(row)
            
        con.close()

    except Exception as ex:
        print('error :' + ex)

    spisok_clients = write_log(path_to_csv)
    spisok_clients.writerow(name_table)
    for data in lst:
        spisok_clients.writerow(data)
